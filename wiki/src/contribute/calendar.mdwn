[[!meta title="Calendar"]]

* 2015-05-02:
  - Feature freeze for Tails 1.4.
  - Translation window starts for Tails 1.4.
  - Build and upload Tails 1.4~rc1 ISO image and IUKs.
  - Start testing Tails 1.4~rc1 during late CEST if building the image
    went smoothly.

* 2015-05-03:
  - [[Monthly meeting|contribute/meetings]]
  - Finish testing Tails 1.4~rc1 by the afternoon, CEST.
  - Release Tails 1.4~rc1 during late CEST.


* 2015-05-11
  - All branches targeting Tails 1.4 must be merged into the 'testing'
    branch by noon CEST.
  - Tor Browser 4.5.x, based on Firefox 31.7.0esr, is hopefully out so
    we can import it.
  - Build and upload Tails 1.4 ISO image and IUKs.
  - Start testing Tails 1.4 during late CEST if building the image
    went smoothly.

* 2015-05-12:
  - [[Low-hanging fruits session|contribute/low-hanging_fruit_sessions]]
  - Finish testing Tails 1.4 by the afternoon, CEST.
  - Release Tails 1.4 during late CES

* 2015-06-03: [[Monthly meeting|contribute/meetings]]

* 2015-06-12: [[Low-hanging fruits session|contribute/low-hanging_fruit_sessions]]

* 2015-06-30: Release 1.4.1
  - anonym is the RM until June 10
  - intrigeri is the RM from June 10 to the release

* 2015-07-03: [[Monthly meeting|contribute/meetings]]

* 2015-07-12: [[Low-hanging fruits session|contribute/low-hanging_fruit_sessions]]

* 2015-08-03: [[Monthly meeting|contribute/meetings]]

* 2015-08-11: Release 1.5
  - intrigeri is the RM until July 20
  - anonym is the RM from July 20 to the release

* 2015-08-12: [[Low-hanging fruits session|contribute/low-hanging_fruit_sessions]]

* 2015-09-03: [[Monthly meeting|contribute/meetings]]

* 2015-09-12: [[Low-hanging fruits session|contribute/low-hanging_fruit_sessions]]

* 2015-09-22: Release 1.5.1 (anonym is the RM)

* 2015-11-03: Release 1.6 (anonym is the RM)

* 2015-12-15: Release 1.6.1 (anonym is the RM)

* 2016-02-02 (?): Release 1.7

* 2016-03-15 (?): Release 1.7.1
