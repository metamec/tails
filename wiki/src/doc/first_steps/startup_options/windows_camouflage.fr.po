# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-07-21 03:22+0300\n"
"PO-Revision-Date: 2014-03-11 17:21-0000\n"
"Last-Translator: amnesia <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Windows camouflage\"]]\n"
msgstr "[[!meta title=\"Camouflage Windows\"]]\n"

#. type: Plain text
msgid ""
"If you are using a computer in public you may want to avoid attracting "
"unwanted attention by changing the way Tails looks into something that "
"resembles Microsoft Windows 8."
msgstr ""
"Si vous utilisez un ordinateur dans un endroit public, vous désirez peut-"
"être éviter d'attirer l'attention, en remplaçant l'apparence de Tails par "
"celle d'un Microsoft Windows 8."

#. type: Title =
#, no-wrap
msgid "Activate the Windows camouflage\n"
msgstr "Activer le camouflage Windows\n"

#. type: Plain text
#, no-wrap
msgid ""
"The Windows camouflage can be activated from [[<span class=\"application\">Tails\n"
"Greeter</span>|startup_options#tails_greeter]]:\n"
msgstr ""
"Le camouflage Windows peut être activé depuis le [[<span class=\"application\">Tails\n"
"Greeter</span>|startup_options#tails_greeter]] :\n"

#. type: Bullet: '1. '
msgid ""
"When <span class=\"application\">Tails Greeter</span> appears, in the <span "
"class=\"guilabel\">Welcome to Tails</span> window, click on the <span class="
"\"button\">Yes</span> button. Then click on the <span class=\"button"
"\">Forward</span> button."
msgstr ""
"Quand le <span class=\"application\">Tails Greeter</span> apparaît, dans la "
"fenêtre <span class=\"guilabel\">Bienvenue dans Tails</span>, cliquez sur le "
"bouton <span class=\"button\">Oui</span>. Cliquez ensuite sur le bouton "
"<span class=\"button\">Suivant</span>."

#. type: Bullet: '2. '
msgid ""
"In the <span class=\"guilabel\">Windows camouflage</span> section, select "
"the <span class=\"guilabel\">Activate Microsoft Windows 8 Camouflage</span> "
"option."
msgstr ""
"Dans la section <span class=\"guilabel\">Camouflage Windows</span>, "
"sélectionnez l'option <span class=\"guilabel\">Activer le camouflage "
"Microsoft Windows 8</span>."

#. type: Plain text
msgid "This is how your Tails desktop will look like:"
msgstr "Voici ce à quoi votre bureau Tails devrait ressembler : "

#. type: Plain text
#, no-wrap
msgid "[[!img windows_camouflage.jpg link=no alt=\"Tails with Windows camouflage\"]]\n"
msgstr "[[!img windows_camouflage.jpg link=no alt=\"Tails avec le camouflage Windows\"]]\n"
